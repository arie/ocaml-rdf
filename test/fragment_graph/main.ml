(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* TODO these tests have grown into a mess. needs some cleaning up. *)

module FragmentGraph = Rdf_fragment_graph.Make (struct
  let hash s =
    "urn:dummy_hash:" ^ string_of_int @@ Hashtbl.hash s |> Rdf.Iri.of_string
end)

module Gen = struct
  let fragment_reference =
    QCheck.Gen.(string_size ~gen:(char_range 'a' 'z') small_nat) |> QCheck.make

  let predicate =
    QCheck.choose
      [
        fragment_reference
        |> QCheck.map FragmentGraph.Predicate.make_fragment_reference;
        Rdf_gen.iri |> QCheck.map FragmentGraph.Predicate.of_iri;
      ]

  let object' =
    QCheck.choose
      [
        fragment_reference
        |> QCheck.map FragmentGraph.Object.make_fragment_reference;
        Rdf_gen.iri |> QCheck.map FragmentGraph.Object.of_iri;
        Rdf_gen.literal |> QCheck.map FragmentGraph.Object.of_literal;
      ]

  let statement = QCheck.pair predicate object'
  let fragment_statement = QCheck.triple fragment_reference predicate object'

  let fragment_graph_add_statements statements fg =
    List.fold_left
      (fun fg (p, o) -> FragmentGraph.add_statement p o fg)
      fg statements

  let fragment_graph_add_fragment_statements fragment_statements fg =
    List.fold_left
      (fun fg (f, p, o) -> FragmentGraph.add_fragment_statement f p o fg)
      fg fragment_statements

  let fragment_graph =
    QCheck.(
      pair (list statement) (list fragment_statement)
      |> map
           ~rev:(fun fragment_graph ->
             ( FragmentGraph.statements fragment_graph |> List.of_seq,
               FragmentGraph.fragment_statements fragment_graph |> List.of_seq
             ))
           (fun (statements, fragment_statements) ->
             FragmentGraph.empty
             |> fragment_graph_add_statements statements
             |> fragment_graph_add_fragment_statements fragment_statements))
end

let fragment_graph_testable =
  Alcotest.testable FragmentGraph.pp FragmentGraph.equal

let predicate_testable = Alcotest.testable FragmentGraph.Predicate.pp ( = )
let ex = Rdf.Namespace.make_namespace "http://example.com/"

let predicate_of_iri_test =
  let open Alcotest in
  test_case "Predicate.of_iri" `Quick (fun () ->
      check predicate_testable "no base_subject returns the iri"
        (FragmentGraph.Predicate.of_iri @@ ex "hello")
        (FragmentGraph.Predicate.of_iri @@ ex "hello");

      check predicate_testable "with base_subject return a fragment identifier"
        (FragmentGraph.Predicate.make_fragment_reference "hello")
        (FragmentGraph.Predicate.of_iri
           ~base_subject:(Rdf.Iri.of_string "http://example.com/")
        @@ ex "#hello");

      Format.printf "%a\n" Rdf.Iri.pp
        (Rdf.Iri.with_fragment (ex "hi") (Some "blup")))

let of_triples_test =
  let open Alcotest in
  test_case "of_triples" `Quick (fun () ->
      let fg_direct =
        FragmentGraph.(
          empty
          |> add_statement
               (Predicate.make_fragment_reference "prop")
               (Object.make_fragment_reference "something")
          |> add_fragment_statement "hi"
               (Predicate.of_iri @@ Rdf.Iri.of_string "urn:example:foo")
               (Object.of_literal @@ Rdf.Literal.make "blups" (ex "type")))
      in

      let fg_of_triples =
        [
          Rdf.Triple.(
            make
              (Subject.of_iri @@ Rdf.Iri.of_string "http://example.com/")
              (Predicate.of_iri @@ ex "#prop")
              (Object.of_iri @@ ex "#something"));
          Rdf.Triple.(
            make
              (Subject.of_iri @@ Rdf.Iri.of_string "http://example.com/#hi")
              (Predicate.of_iri @@ Rdf.Iri.of_string "urn:example:foo")
              (* TODO! an intersting case I had not thought about.
                 What if the literal datatype makes a reference to the
                 base_subject? *)
              (Object.of_literal @@ Rdf.Literal.make "blups" (ex "type")));
        ]
        |> List.to_seq |> FragmentGraph.of_triples |> List.of_seq |> List.hd
        |> fun (_, fg) -> fg
      in

      check fragment_graph_testable "of_triples and directly creating is equal"
        fg_direct fg_of_triples)

let equal_unit_test =
  let open Alcotest in
  test_case "unit tests" `Quick (fun () ->
      check bool "empty fragment graphs are equal" true
        FragmentGraph.(equal empty empty);

      check bool
        "empty fragment graph is not equal to a fragment graph with one \
         statement"
        false
        FragmentGraph.(
          equal empty
            FragmentGraph.(
              empty
              |> add_statement
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar")));

      check bool "fragment graph with one statement is equal" true
        FragmentGraph.(
          equal
            FragmentGraph.(
              empty
              |> add_statement
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar"))
            FragmentGraph.(
              empty
              |> add_statement
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar")));

      check bool "fragment graph with one statement is equal" true
        FragmentGraph.(
          equal
            (empty
            |> add_statement
                 (Predicate.of_iri @@ ex "foo")
                 (Object.of_literal @@ Rdf.Literal.make "hi" (ex "blups"))
            |> add_statement
                 (Predicate.of_iri @@ ex "foo")
                 (Object.of_literal @@ Rdf.Literal.make "hi" (ex "blups")))
            (empty
            |> add_statement
                 (Predicate.of_iri @@ ex "foo")
                 (Object.of_literal @@ Rdf.Literal.make "hi" (ex "blups"))));

      check bool "fragment graph with different statements is not equal" false
        FragmentGraph.(
          equal
            FragmentGraph.(
              empty
              |> add_statement
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar"))
            FragmentGraph.(
              empty
              |> add_statement
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar2")));

      check bool
        "fragment graph with one fragment statements is equal to itself" true
        FragmentGraph.(
          equal
            FragmentGraph.(
              empty
              |> add_fragment_statement "hi"
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar"))
            FragmentGraph.(
              empty
              |> add_fragment_statement "hi"
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar")));

      check bool
        "fragment graphs with different fragment statements are not equal" false
        FragmentGraph.(
          equal
            FragmentGraph.(
              empty
              |> add_fragment_statement "hi"
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar"))
            FragmentGraph.(
              empty
              |> add_fragment_statement "hi"
                   (Predicate.of_iri @@ ex "foo")
                   (Object.of_iri @@ ex "bar2")));

      check bool "adding same literal twice results in equal fragment graph"
        true
        FragmentGraph.(
          equal
            (empty
            |> add_statement
                 (Predicate.of_iri @@ ex "prop")
                 (Object.of_literal @@ Rdf.Literal.make "hi" (ex "type")))
            (empty
            |> add_statement
                 (Predicate.of_iri @@ ex "prop")
                 (Object.of_literal @@ Rdf.Literal.make "hi" (ex "type"))
            |> add_statement
                 (Predicate.of_iri @@ ex "prop")
                 (Object.of_literal
                 @@ Rdf.Literal.make
                      (* do some transformations to make sure it is not physically equal *)
                      (String.lowercase_ascii (String.uppercase_ascii "hi"))
                      (ex "type")))))

let shuffle lst =
  let shuffle arr =
    for n = Array.length arr - 1 downto 1 do
      let k = Random.int (n + 1) in
      let temp = arr.(n) in
      arr.(n) <- arr.(k);
      arr.(k) <- temp
    done
  in
  let array = Array.of_list lst in
  shuffle array;
  Array.to_list array

let equal_property_test =
  QCheck.Test.make
    ~name:
      "equality of fragment graphs when order of adding statements is shuffled"
    (QCheck.pair
       (QCheck.list Gen.statement)
       (QCheck.list Gen.fragment_statement))
    (fun (statements, fragment_statements) ->
      let a =
        FragmentGraph.empty
        |> Gen.fragment_graph_add_statements statements
        |> Gen.fragment_graph_add_fragment_statements fragment_statements
      in
      (* shuffle the order of insertions in b *)
      let b =
        FragmentGraph.empty
        |> Gen.fragment_graph_add_statements (shuffle statements)
        |> Gen.fragment_graph_add_fragment_statements
             (shuffle fragment_statements)
      in
      FragmentGraph.equal a b)

let canoncial_encode_decode_test =
  QCheck.Test.make ~name:"encode/decode canonical serialization"
    Gen.fragment_graph (fun fragment_graph ->
      let b =
        fragment_graph |> FragmentGraph.canonical |> FragmentGraph.of_canonical
        |> Result.get_ok
      in
      Alcotest.check fragment_graph_testable
        "encoded/decoded value is same as original" fragment_graph b;
      true)

let canonical_unit_test =
  let check test_label fragment_graph =
    Alcotest.check fragment_graph_testable test_label fragment_graph
      (fragment_graph |> FragmentGraph.canonical |> FragmentGraph.of_canonical
     |> Result.get_ok)
  in
  Alcotest.test_case "unit tests" `Quick (fun () ->
      check "empty" FragmentGraph.empty;

      (check "single statement"
      @@ FragmentGraph.(
           empty
           |> add_statement
                (Predicate.of_iri @@ ex "foo")
                (Object.of_iri @@ ex "bar")));

      (check "single fragment statement"
      @@ FragmentGraph.(
           empty
           |> add_fragment_statement "hi"
                (Predicate.of_iri @@ ex "foo")
                (Object.of_iri @@ ex "bar")));

      check "single fragment statement"
      @@ FragmentGraph.(
           empty
           |> add_statement
                (Predicate.of_iri @@ ex "foo")
                (Object.of_iri @@ ex "bar")
           |> add_fragment_statement "hi"
                (Predicate.of_iri @@ ex "foo")
                (Object.of_literal
                @@ Rdf.(Literal.make "hi" @@ Namespace.xsd "something"))))

let () =
  Alcotest.run "rdf_fragment_graph"
    [
      ("constructors", [ predicate_of_iri_test; of_triples_test ]);
      ( "equal",
        [ equal_unit_test; QCheck_alcotest.to_alcotest equal_property_test ] );
      ( "canonical serializaton",
        [
          canonical_unit_test;
          QCheck_alcotest.to_alcotest canoncial_encode_decode_test;
        ] );
    ]
