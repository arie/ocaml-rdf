(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Triple = Rdf.Triple

(* Check whether two lists have the same elements *)
let list_equal_up_to_order equal l1 l2 =
  l1
  |> List.fold_left
       (fun acc y ->
         if not acc then false
         else if not (List.exists (fun x -> equal y x) l2) then false
         else true)
       true
  && l2
     |> List.fold_left
          (fun acc y ->
            if not acc then false
            else if not (List.exists (fun x -> equal y x) l1) then false
            else true)
          true

let encode_decode_subject =
  QCheck.Test.make ~count:1000 ~name:"decode/encode subject" Rdf_gen.subject
    (fun subject ->
      Alcotest.check Rdf_alcotest.subject "decode/encode" subject
        (subject |> Rdf_json.encode_subject |> Rdf_json.decode_subject);
      true)

let encode_decode_predicate =
  QCheck.Test.make ~count:1000 ~name:"decode/encode predicate" Rdf_gen.predicate
    (fun predicate ->
      Alcotest.check Rdf_alcotest.predicate "decode/encode" predicate
        (predicate |> Rdf_json.encode_predicate |> Rdf_json.decode_predicate);
      true)

let encode_decode_object =
  QCheck.Test.make ~count:1000 ~name:"decode/encode object" Rdf_gen.object'
    (fun object' ->
      Alcotest.check Rdf_alcotest.object' "decode/encode" object'
        (object' |> Rdf_json.encode_object |> Rdf_json.decode_object);
      true)

(* The test uses the 'list_equal_up_to_order' function, which checks whether two lists contain the same elements *)
(* (so it evaluates to true when the order is different). This is because the decode / encode does not preserve the order. *)
(* Hence the form of the test is different from the ones above, which use a 'testable'. *)
(* To make a testable for lists of triples, we would need an equal function. *)

let encode_decode_triple_list =
  let triple_list_arbitrary = Rdf_gen.triple |> QCheck.small_list in
  QCheck.Test.make ~count:1000 ~name:"decode encode small list"
    triple_list_arbitrary (fun lst ->
      list_equal_up_to_order Triple.equal lst
        (lst |> Rdf_json.encode |> Rdf_json.decode))

let () =
  Alcotest.run "Json"
    [
      ( "Encoder",
        List.map QCheck_alcotest.to_alcotest
          [
            encode_decode_triple_list;
            encode_decode_subject;
            encode_decode_predicate;
            encode_decode_object;
          ] );
    ]
