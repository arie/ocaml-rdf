(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Iri = Rdf.Iri
module Blank_node = Rdf.Blank_node
module Literal = Rdf.Literal
module Term = Rdf.Term
module Triple = Rdf.Triple
module Subject = Triple.Subject
module Predicate = Triple.Predicate
module Object = Triple.Object
module Namespace = Rdf.Namespace

let char_name = QCheck.Gen.(char_range (Char.chr 0x61) (Char.chr 0x7A))
let name = QCheck.string_gen char_name
let short_id = QCheck.string_gen_of_size QCheck.Gen.small_nat char_name

let iri =
  QCheck.always "https://example.com"
  |> QCheck.map ~rev:Rdf.Iri.to_string Rdf.Iri.of_string
(* QCheck.Gen.(
 *   (fun scheme host -> Iri.make ~scheme ~host ())
 *   <$> oneofl [ "http"; "https" ]
 *   <*> oneofl [ "example.com"; "test.com"; "dream.cat" ])
 * |> QCheck.make ~print:Iri.to_string *)

let blank_node =
  QCheck.map ~rev:Blank_node.identifier Blank_node.of_string short_id

let literal =
  QCheck.(
    choose
      [
        map ~rev:Rdf.Literal.canonical Rdf.Literal.make_string short_id;
        map
          ~rev:(fun literal -> Rdf.Literal.canonical literal |> int_of_string)
          Rdf.Literal.make_integer int;
      ])

let term =
  QCheck.(
    [
      iri |> map Term.of_iri;
      blank_node |> map Term.of_blank_node;
      literal |> map Term.of_literal;
    ]
    |> choose)

let subject =
  QCheck.(
    [ iri |> map Subject.of_iri; blank_node |> map Subject.of_blank_node ]
    |> choose)

let predicate = iri |> QCheck.map ~rev:Predicate.to_iri Predicate.of_iri

let object' =
  QCheck.(
    [
      iri |> map Object.of_iri;
      blank_node |> map Object.of_blank_node;
      literal |> map Object.of_literal;
    ]
    |> choose)

let triple =
  QCheck.triple subject predicate object'
  |> QCheck.map (fun (s, p, o) -> Triple.make s p o)

let triples = QCheck.list triple

let graph =
  triple |> QCheck.small_list
  |> QCheck.map
       ~rev:(fun graph -> Rdf.Graph.to_triples graph |> List.of_seq)
       (fun triples ->
         Rdf.Graph.empty |> Rdf.Graph.add_seq (List.to_seq triples))

let rdf = QCheck.string |> QCheck.map Namespace.rdf
let rdfs = QCheck.string |> QCheck.map Namespace.rdfs
let owl = QCheck.string |> QCheck.map Namespace.owl
let xsd = QCheck.string |> QCheck.map Namespace.xsd
