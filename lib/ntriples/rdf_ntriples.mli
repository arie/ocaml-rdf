(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 *)

(** Parser for the RDF 1.1 N-Triples serialization.

@see <https://www.w3.org/TR/n-triples/>

 *)

val parse : char Seq.t -> Rdf.Triple.t Seq.t
(** [parse chars] reads a sequence of characters that forms an N-Triples document and returns a sequence of triples. *)

val encode : Rdf.Triple.t -> string
(** [encode triples] returns the encoding of the triple to a N-Triples line. *)
