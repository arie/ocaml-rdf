(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 * SPDX-FileCopyrightText: 2022 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2012-2021 Institut National de Recherche en Informatique et en Automatique
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This file contains parts copied from the file `ttl_lex.ml` of the GPL-3.0 licensed ocaml-rdf library by Zoggy (https://framagit.org/zoggy/ocaml-rdf).
 *
 *)

module Lexer = struct
  let is_string_escaped_char = function
    | 'b' | 'f' | 'n' | 'r' | 't' | '"' | '\'' | '\\' -> true
    | _ -> false

  let string_escaped_chars_map = function
    | 'b' -> '\b'
    | 'f' -> Char.chr 0x0c
    | 'n' -> '\n'
    | 'r' -> '\r'
    | 't' -> '\t'
    | '"' -> '"'
    | '\'' -> '\''
    | '\\' -> '\\'
    | c -> c

  let is_escaped_uchar uc =
    try uc |> Uchar.to_char |> is_string_escaped_char with _ -> false

  (* here uc is an escaped character, so it's ok to use Uchar.to_char *)
  let escaped_uchars_map uc =
    uc |> Uchar.to_char |> string_escaped_chars_map |> Uchar.of_char

  (* This simply transforms a string into a sequence of Uchar's *)
  let decode s =
    Uutf.String.fold_utf_8
      (fun res _pos v ->
        match v with
        | `Malformed s ->
            raise
            @@ Invalid_argument
                 (Format.sprintf
                    "Malformed UTF-8 while checking rdf:ID value (%s)" s)
        | `Uchar uchar -> Seq.append res (Seq.return uchar))
      Seq.empty s

  (* we assume that we have ASCII chars here. No problem if we fail *)
  let int_of_chars seq len =
    (* take first len uchars, and turn into a string *)
    let s =
      Seq.fold_left
        (fun s c -> s ^ String.make 1 (Uchar.to_char c))
        "" (Seq.take len seq)
    in
    let s2 = "0x" ^ s in
    try int_of_string s2
    with _ -> raise @@ Invalid_argument ("Invalid UTF8 code: " ^ s)

  (* we use this function after we already encountered a '\' *)
  let match_on_escape_numeric seq b =
    let len = Seq.length seq in
    match seq () with
    (* match on u and ensure there are at least 4 more chars *)
    | Seq.Cons (i, tail) when Uchar.of_char 'u' = i && len > 4 ->
        let d = int_of_chars tail 4 |> Uchar.of_int in
        let tail' = Seq.drop 4 tail in
        let () = Buffer.add_utf_8_uchar b d in
        tail'
    (* match on U and ensure there are at least 8 more chars *)
    | Seq.Cons (i, tail) when Uchar.of_char 'U' = i && len > 8 ->
        let d = int_of_chars tail 8 |> Uchar.of_int in
        let tail' = Seq.drop 8 tail in
        let () = Buffer.add_utf_8_uchar b d in
        tail'
    (* A '\' is only allowed if it is followed by a numeric sequence *)
    | Seq.Cons (i, _tail) ->
        raise
        @@ Invalid_argument
             (Fmt.str "unexpected char: %c after \\" (Uchar.to_char i))
    | Seq.Nil ->
        raise @@ Invalid_argument (Fmt.str "unexpected: no char after \\")

  (* we use this function after we already encountered a '\' *)
  let match_on_escape_numeric_and_string seq b =
    let len = Seq.length seq in
    match seq () with
    (* match on escaped characters *)
    | Seq.Cons (i, tail) when is_escaped_uchar i ->
        let d = escaped_uchars_map i in
        let () = Buffer.add_utf_8_uchar b d in
        tail
    (* match on u and ensure there are at least 4 more chars *)
    | Seq.Cons (i, tail) when Uchar.of_char 'u' = i && len > 4 ->
        let d = int_of_chars tail 4 |> Uchar.of_int in
        let tail' = Seq.drop 4 tail in
        let () = Buffer.add_utf_8_uchar b d in
        tail'
    (* match on U and ensure there are at least 8 more chars *)
    | Seq.Cons (i, tail) when Uchar.of_char 'U' = i && len > 8 ->
        let d = int_of_chars tail 8 |> Uchar.of_int in
        let tail' = Seq.drop 8 tail in
        let () = Buffer.add_utf_8_uchar b d in
        tail'
    (* A '\' is only allowed if it is followed by a numeric sequence or
     * a string escape *)
    | Seq.Cons (i, tail) ->
        let () = Buffer.add_utf_8_uchar b i in
        tail
    | Seq.Nil ->
        raise @@ Invalid_argument (Fmt.str "unexpected: no char after \\")

  let unescape match_on_escape s =
    let rec iter seq b =
      match seq () with
      (* match on \ *)
      | Seq.Cons (i, tail) when Uchar.of_char '\\' = i ->
          let tail' = match_on_escape tail b in
          iter tail' b
      (* anything else, just add to buffer and continue *)
      | Seq.Cons (i, tail) ->
          let () = Buffer.add_utf_8_uchar b i in
          iter tail b
      | Seq.Nil -> Seq.empty
    in
    let len = String.length s in
    let b = Buffer.create len in
    let seq = decode s in
    let _ = iter seq b () in
    Buffer.contents b

  (* for literals *)
  let unescape_numeric_and_string = unescape match_on_escape_numeric_and_string

  (* for iri's *)
  let unescape_numeric = unescape match_on_escape_numeric
  let digit = [%sedlex.regexp? '0' .. '9']
  let alpha = [%sedlex.regexp? 'a' .. 'z' | 'A' .. 'Z']
  let hex = [%sedlex.regexp? digit | 'A' .. 'F' | 'a' .. 'f']

  let pn_chars_base =
    [%sedlex.regexp?
      ( alpha
      | 0x00C0 .. 0x00D6
      | 0x00D8 .. 0x00F6
      | 0x00F8 .. 0x02FF
      | 0x0370 .. 0x037D
      | 0x037F .. 0x1FFF
      | 0x200C .. 0x200D
      | 0x2070 .. 0x218F
      | 0x2C00 .. 0x2FEF
      | 0x3001 .. 0xD7FF
      | 0xF900 .. 0xFDCF
      | 0xFDF0 .. 0xFFFD
      | 0x10000 .. 0xEFFFF )]

  let pn_chars_u = [%sedlex.regexp? pn_chars_base | '_']

  let pn_chars =
    [%sedlex.regexp?
      pn_chars_u | '-' | digit | 0x00B7 | 0x0300 .. 0x036F | 0x203F .. 0x2040]

  let pn_prefix =
    [%sedlex.regexp? pn_chars_base, Opt (Star (pn_chars | '.'), pn_chars)]

  let percent = [%sedlex.regexp? '%', hex, hex]

  let pn_local_sec =
    [%sedlex.regexp?
      ( '\\',
        ( '_' | '~' | '.' | '-' | '!' | '$' | '&' | "'" | '(' | ')' | '*' | '+'
        | ',' | ';' | '=' | '/' | '?' | '#' | '@' | '%' ) )]

  let plx = [%sedlex.regexp? percent | pn_local_sec]

  let pn_local =
    [%sedlex.regexp?
      ( (pn_chars_u | ':' | digit | plx),
        Opt (Star (pn_chars | '.' | ':' | plx), (pn_chars | ':' | plx)) )]

  let uchar =
    [%sedlex.regexp?
      "\\u", hex, hex, hex, hex | "\\U", hex, hex, hex, hex, hex, hex, hex, hex]

  let iriref =
    [%sedlex.regexp?
      '<', Star (Compl (0x00 .. 0x20 | Chars "<>\\\"{}|^`") | uchar), '>']

  let pname_ns = [%sedlex.regexp? Opt pn_prefix, ':']
  let pname_ln = [%sedlex.regexp? pname_ns, pn_local]

  let blank_node_label =
    [%sedlex.regexp?
      "_:", (pn_chars_u | digit), Opt (Star (pn_chars | '.'), pn_chars)]

  let langtag =
    [%sedlex.regexp? '@', Plus alpha, Star ('-', Plus (alpha | digit))]

  let integer = [%sedlex.regexp? Opt ('+' | '-'), Plus digit]
  let decimal = [%sedlex.regexp? Opt ('+' | '-'), Star digit, '.', Plus digit]
  let exponent = [%sedlex.regexp? ('e' | 'E'), Opt ('+' | '-'), Plus digit]

  let double =
    [%sedlex.regexp?
      ( Opt ('+' | '-'),
        ( Plus digit, '.', Star digit, exponent
        | '.', Plus digit, exponent
        | Plus digit, exponent ) )]

  let echar =
    [%sedlex.regexp? '\\', ('t' | 'b' | 'n' | 'r' | 'f' | '\\' | '"' | '\\')]

  let string_literal_quote =
    [%sedlex.regexp?
      '"', Star (Compl (0x22 | 0x5C | 0xA | 0xD) | echar | uchar), '"']

  let string_literal_single_quote =
    [%sedlex.regexp?
      "'", Star (Compl (0x27 | 0x5C | 0xA | 0xD) | echar | uchar), "'"]

  let string_literal_long_single_quote =
    [%sedlex.regexp?
      "'''", Star (Opt ("'" | "''"), Compl ('\'' | '\\') | echar | uchar), "'''"]

  let string_literal_long_quote =
    [%sedlex.regexp?
      ( "\"\"\"",
        Star (Opt ('"' | "\"\""), Compl ('"' | '\\') | echar | uchar),
        "\"\"\"" )]

  let ws = [%sedlex.regexp? 0x20 | 0x9 | 0xD | 0xA]
  let anon = [%sedlex.regexp? '[', Star ws, ']']
  let comment = [%sedlex.regexp? '#', Star (Compl (0xA | 0xD))]
  let boolean = [%sedlex.regexp? "true" | "false"]

  type token =
    | IriRef of string
    | StringLiteralQuote of string
    | LangTag of string
    | BlankNodeLabel of string
    | HatHat
    | Dot

  let rec main buf =
    match%sedlex buf with
    | white_space -> main buf
    | "\r\n" -> main buf
    | '\r' -> main buf
    | '\n' -> main buf
    | comment -> main buf
    | '.' -> Some Dot
    | "^^" -> Some HatHat
    | iriref ->
        let s = Sedlexing.Utf8.lexeme buf in
        let iri = String.sub s 1 (String.length s - 2) in
        let iri = unescape_numeric iri in
        Some (IriRef iri)
    | blank_node_label ->
        let s = Sedlexing.Utf8.lexeme buf in
        let id = String.sub s 2 (String.length s - 2) in
        Some (BlankNodeLabel id)
    | string_literal_quote ->
        let s = Sedlexing.Utf8.lexeme buf in
        let s = String.sub s 1 (String.length s - 2) in
        let s = unescape_numeric_and_string s in
        Some (StringLiteralQuote s)
    | langtag ->
        let s = Sedlexing.Utf8.lexeme buf in
        let s = String.sub s 1 (String.length s - 1) in
        Some (LangTag s)
    | eof -> None
    | _ -> raise @@ Invalid_argument "Unexpected token while parsing N-Triples."

  let rec tokenize lexbuf () =
    match main lexbuf with
    | Some token -> Seq.Cons (token, tokenize lexbuf)
    | None -> Seq.Nil
end

module Parser = struct
  (* This is a parse combinator that works on a sequence of tokens. *)

  (* TODO: Add the Lexer position to error messages. *)

  type tokens = Lexer.token Seq.t
  type 'a t = tokens -> ('a * tokens, string) Result.t

  let return v seq = Ok (v, seq)
  (* let fail msg = Error msg *)

  let map p f seq =
    match p seq with Ok (v, rest) -> Ok (f v, rest) | Error msg -> Error msg

  let bind (p : 'a t) (f : 'a -> 'b t) seq =
    match p seq with Ok (v, rest) -> (f v) rest | Error msg -> Error msg

  let ( >>= ) p f = bind p f
  let ( >>| ) p f = map p f
  let ( *> ) a b = a >>= fun _ -> b

  let ( <|> ) p q seq =
    match p seq with Ok (v, rest) -> Ok (v, rest) | Error _ -> q seq

  let iri seq =
    match seq () with
    | Seq.Cons (Lexer.IriRef iri, rest) -> (
        (* Hack to determine whether the iri is relative, which is not allowed
         * for ntriples *)
        match iri |> Rdf.Iri.of_string |> Rdf.Iri.scheme with
        | Some _ -> Ok (Rdf.Iri.of_string iri, rest)
        | None -> raise @@ Invalid_argument "Iri has no scheme")
    | _ -> Error "expecting IriRef"

  let bnode seq =
    match seq () with
    | Seq.Cons (Lexer.BlankNodeLabel id, rest) ->
        Ok (Rdf.Blank_node.of_string id, rest)
    | _ -> Error "expecting BlankNodeLabel"

  let string_literal_quote seq =
    match seq () with
    | Seq.Cons (Lexer.StringLiteralQuote s, rest) -> Ok (s, rest)
    | _ -> Error "expecting StringLiteralQuote"

  let lang_tag seq =
    match seq () with
    | Seq.Cons (Lexer.LangTag s, rest) -> Ok (s, rest)
    | _ -> Error "expecting LangTag"

  let hat_hat seq =
    match seq () with
    | Seq.Cons (Lexer.HatHat, rest) -> Ok ((), rest)
    | _ -> Error "expecting HatHat"

  let literal =
    string_literal_quote >>= fun s ->
    lang_tag
    >>| (fun language -> Rdf.Literal.make_string ~language s)
    <|> (hat_hat *> iri >>| fun iri -> Rdf.Literal.make s iri)
    <|> return @@ Rdf.Literal.make_string s

  let subject =
    iri >>| Rdf.Triple.Subject.of_iri
    <|> (bnode >>| Rdf.Triple.Subject.of_blank_node)

  let predicate = iri >>| Rdf.Triple.Predicate.of_iri

  let object' =
    iri >>| Rdf.Triple.Object.of_iri
    <|> (bnode >>| Rdf.Triple.Object.of_blank_node)
    <|> (literal >>| Rdf.Triple.Object.of_literal)

  let dot seq =
    match seq () with
    | Seq.Cons (Lexer.Dot, rest) -> Ok ((), rest)
    | _ -> Error "expecting Dot"

  let eof seq =
    match seq () with
    | Seq.Nil -> Ok (None, Seq.empty)
    | _ -> raise @@ Invalid_argument "expecting eof"

  let triple_dot =
    subject
    >>= (fun s ->
          predicate >>= fun p ->
          object' >>= fun o ->
          dot >>| fun () -> Some (Rdf.Triple.make s p o))
    <|> eof
end

let parse char_seq =
  let tokens =
    char_seq
    (* memoize char sequence - if not this cause reads over the
       end. Don't know exactly what is causing it. Gen.of_seq and
       `from_gen` should be able to hand ephermal sequences. *)
    |> Seq.memoize
    (* create a Sedlex buffer *)
    |> Gen.of_seq
    |> Sedlexing.Utf8.from_gen
    (* tokenize *)
    |> Lexer.tokenize
    (* make sequence persistent in order to be able to backtrace when parsing *)
    |> Seq.memoize
  in
  let rec do_parse tokens () =
    match Parser.triple_dot tokens with
    | Ok (Some triple, tokens_rest) -> Seq.Cons (triple, do_parse tokens_rest)
    | Ok (None, _) -> Seq.Nil
    | Error msg -> failwith msg
  in
  do_parse tokens

(* Encode *)

let encode_iri iri = "<" ^ Rdf.Iri.to_string iri ^ ">"
let encode_bnode bnode = "_:" ^ Rdf.Blank_node.identifier bnode

let encode_literal literal =
  match Rdf.Literal.language literal with
  | Some language -> "\"" ^ Rdf.Literal.canonical literal ^ "\"@" ^ language
  | None ->
      let datatype = Rdf.Literal.datatype literal in
      if Rdf.Iri.equal (Rdf.Namespace.xsd "string") datatype then
        "\"" ^ Rdf.Literal.canonical literal ^ "\""
      else "\"" ^ Rdf.Literal.canonical literal ^ "\"^^" ^ encode_iri datatype

let encode_term term = Rdf.Term.map encode_iri encode_bnode encode_literal term

let encode (triple : Rdf.Triple.t) =
  encode_term (Rdf.Triple.Subject.to_term triple.subject)
  ^ " "
  ^ encode_term (Rdf.Triple.Predicate.to_term triple.predicate)
  ^ " "
  ^ encode_term (Rdf.Triple.Object.to_term triple.object')
  ^ " ."
