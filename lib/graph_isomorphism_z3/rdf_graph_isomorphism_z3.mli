(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** {1 RDF Graph Isomorphism with Z3} 

  This module uses the Z3 Theorem Prover for deciding RDF Graph isomorphism.

@see <https://github.com/Z3prover/z3>

*)

val isomorphic : Rdf.Graph.t -> Rdf.Graph.t -> bool
(** [isomorphic graph_a graph_b] returns true if [a] and [b] are isomorphic. 

Note that this is very slow and only intended for passing the test cases.

@see <https://www.w3.org/TR/rdf11-concepts/#graph-isomorphism>
*)
