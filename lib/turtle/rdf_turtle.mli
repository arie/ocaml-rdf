(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 * SPDX-FileCopyrightText: 2022 alleycat <arie@alleycat.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** {1 Parser} *)

val parse :
  ?base_iri:Rdf.Iri.t ->
  ?seed:Random.State.t ->
  f:(Rdf.Triple.t -> unit) ->
  char Seq.t ->
  unit
(** [parse ~base_iri ~seed ~f seq] reads a sequence of characters, which forms a Turtle document, transforms it to a sequence of Turtle statements, reads this sequence and on producing an Rdf.Triple, it envokes [f]. See https://www.w3.org/TR/turtle/#sec-parsing. *)

val parse_to_graph :
  ?base_iri:Rdf.Iri.t -> ?seed:Random.State.t -> char Seq.t -> Rdf.Graph.t
(** [parse_to_graph ~base_iri ~seed seq] reads the sequance of chars and builds a graph holding all Rdf.triples appearing in the Turtle document. *)
