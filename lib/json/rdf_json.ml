(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Triple = Rdf.Triple
module Term = Rdf.Term
module Blank_node = Rdf.Blank_node
module Literal = Rdf.Literal
module Subject = Triple.Subject
module Predicate = Triple.Predicate
module Object = Triple.Object

(* TODO is this ok? There is List.sort_uniq, but that needs a compare function *)
let rec remove_dups lst =
  match lst with
  | [] -> []
  | h :: t -> h :: remove_dups (List.filter (fun x -> x <> h) t)

let string_starts_with s subs =
  if String.length subs > String.length s then false
  else if subs <> String.sub s 0 (String.length subs) then false
  else true

let json_from_string str = `String str
let json_from_assoc assoc = `Assoc assoc
let json_from_list lst = `List lst

(* TODO we don't really need this, you can just write triple.subject when necessary *)
let subject_of_triple : Triple.t -> Subject.t = fun triple -> triple.subject

let predicate_of_triple : Triple.t -> Predicate.t =
 fun triple -> triple.predicate

let object_of_triple : Triple.t -> Object.t = fun triple -> triple.object'

let encode_subject s =
  Subject.map
    (fun iri -> Uri.to_string iri)
    (fun bnode -> "_:" ^ Blank_node.identifier bnode)
    s

let encode_predicate p = Predicate.map (fun iri -> Uri.to_string iri) p

let encode_object o =
  Object.map
    (fun iri ->
      [
        ("type", "uri" |> json_from_string);
        ("value", Uri.to_string iri |> json_from_string);
      ]
      |> json_from_assoc)
    (fun bnode ->
      [
        ("type", "bnode" |> json_from_string);
        ("value", "_:" ^ Blank_node.identifier bnode |> json_from_string);
      ]
      |> json_from_assoc)
    (fun literal ->
      [
        ("type", "literal" |> json_from_string);
        ("value", Literal.canonical literal |> json_from_string);
        ( "datatype",
          literal |> Literal.datatype |> Uri.to_string |> json_from_string );
      ]
      |> (fun lst ->
           match Literal.language literal with
           | Some l -> ("lang", l |> json_from_string) :: lst
           | None -> lst)
      |> json_from_assoc)
    o

let subject_predicates : Triple.t list -> Subject.t -> Predicate.t list =
 fun lst s ->
  lst
  |> List.filter (fun t -> subject_of_triple t = s)
  |> List.map predicate_of_triple

let subject_predicate_objects :
    Triple.t list -> Subject.t -> Predicate.t -> Object.t list =
 fun lst s p ->
  lst
  |> List.filter (fun t -> subject_of_triple t = s && predicate_of_triple t = p)
  |> List.map object_of_triple

let subjects_of_triples : Triple.t list -> Subject.t list =
 fun ls -> ls |> List.map (fun t -> subject_of_triple t) |> remove_dups

let encode lst =
  lst |> subjects_of_triples
  |> List.map (fun s ->
         ( encode_subject s,
           subject_predicates lst s
           |> List.map (fun p ->
                  ( encode_predicate p,
                    subject_predicate_objects lst s p
                    |> List.map (fun o -> o |> encode_object)
                    |> json_from_list ))
           |> json_from_assoc ))
  |> json_from_assoc

let decode_object json =
  let open Yojson.Basic.Util in
  let value = json |> member "value" |> to_string in
  let type' = json |> member "type" |> to_string in
  match type' with
  | "uri" -> value |> Uri.of_string |> Object.of_iri
  | "bnode" ->
      String.sub value 2 (String.length value - 2)
      |> Blank_node.of_string |> Object.of_blank_node
  | "literal" ->
      let datatype = json |> member "datatype" |> to_string |> Uri.of_string in
      let lang_opt = json |> member "lang" |> to_option to_string in
      let literal =
        match lang_opt with
        | Some lang -> Literal.make_string ~language:lang value
        | None -> Literal.make value datatype
      in
      literal |> Object.of_literal
  | _ -> raise @@ Invalid_argument "wrong json-object"

let decode_predicate str = str |> Uri.of_string |> Predicate.of_iri

let decode_subject str =
  let len = String.length str in
  if string_starts_with str "_:" then
    String.sub str 2 (len - 2) |> Blank_node.of_string |> Subject.of_blank_node
  else str |> Uri.of_string |> Subject.of_iri

let predicate_object : string -> string * Yojson.Basic.t -> Triple.t list =
 fun subj_str (pred_str, object_json) ->
  let pred = decode_predicate pred_str in
  let subj = decode_subject subj_str in
  object_json |> Yojson.Basic.Util.to_list |> List.map decode_object
  |> List.map (fun obj -> Triple.make subj pred obj)

let subject_object : string * Yojson.Basic.t -> Triple.t list =
 fun (subj_str, json) ->
  json |> Yojson.Basic.Util.to_assoc
  |> List.map (predicate_object subj_str)
  |> List.concat

let decode json =
  json |> Yojson.Basic.Util.to_assoc |> List.map subject_object |> List.concat
