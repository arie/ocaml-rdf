(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Triple = Rdf.Triple
module Subject = Triple.Subject
module Predicate = Triple.Predicate
module Object = Triple.Object

val encode_subject : Subject.t -> string
val encode_predicate : Predicate.t -> string
val encode_object : Object.t -> Yojson.Basic.t
val decode_subject : string -> Subject.t
val decode_predicate : string -> Predicate.t
val decode_object : Yojson.Basic.t -> Object.t
val encode : Rdf.Triple.t list -> Yojson.Basic.t
val decode : Yojson.Basic.t -> Rdf.Triple.t list
