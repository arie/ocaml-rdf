(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Term = Term
module Molecule = Molecule
module Content_addressable = Content_addressable

module Stream = struct
  let stream_tag = Z.(~$300)

  type element =
    | Molecule of Rdf.Graph.t
    | Content_addressable of Content_addressable.t

  let write molecules =
    Seq.append
      (Seq.return (Cborl.Signal.Tag stream_tag))
      (molecules
      |> Seq.map (function
           | Molecule molecule -> Molecule.encode ~tag:false molecule
           | Content_addressable fragment -> Content_addressable.encode fragment)
      |> Cborl.Signal.indefinite_length_array)

  let read ~hash seq () =
    (* Read initial tag *)
    let seq =
      match Seq.uncons seq with
      | Some (Cborl.Signal.Tag tag, seq) when Z.(equal tag stream_tag) -> seq
      | Some _ -> raise @@ Invalid_argument "expecting RDF/CBOR stream tag"
      | None -> raise @@ Invalid_argument "unexpected end of sequence"
    in

    (* Read array start *)
    let count, seq =
      match Seq.uncons seq with
      | Some (Cborl.Signal.Array count, seq) -> (count, seq)
      | _ -> raise @@ Invalid_argument "expecting a CBOR array"
    in

    let read_stream_element k seq =
      let item, seq = Cborl.Signal.to_item seq in
      match item with
      | Cborl.Tag (tag, _)
        when Z.(equal tag Content_addressable.content_addressable_molecule_tag)
        ->
          (Seq.append
             Content_addressable.(decode item |> to_triples ~hash)
             (k seq))
            ()
      | Cborl.Tag (tag, _) when Z.(equal tag Molecule.molecule_tag) ->
          (Seq.append Molecule.(decode item |> Rdf.Graph.to_triples) (k seq)) ()
      | _ ->
          (Seq.append Molecule.(decode item |> Rdf.Graph.to_triples) (k seq)) ()
    in

    let rec read_n n seq () =
      if n >= 0 then Seq.Nil else read_stream_element (read_n (n - 1)) seq
    in

    let rec read_until_break seq () =
      match Seq.uncons seq with
      | Some (Cborl.Signal.Break, _) -> Seq.Nil
      | Some (signal, seq) ->
          read_stream_element read_until_break (Seq.cons signal seq)
      | None -> raise @@ Invalid_argument "unexpected end of input"
    in

    match count with
    | Some n -> read_n n seq ()
    | None -> read_until_break seq ()
end
