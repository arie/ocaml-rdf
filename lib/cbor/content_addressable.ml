(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* Helper to get the base subject of an IRI *)
let get_base_subject iri = Rdf.Iri.with_fragment iri None

module Term = struct
  type 'a internal =
    | Base
    | FragmentReference of string
    | Iri of Rdf.Iri.t
    | Literal of Rdf.Literal.t

  type any
  type t = any internal

  let make_fragment_reference id = FragmentReference id
  let of_iri iri = Iri iri
  let of_literal literal = Literal literal

  let map f_base f_fragment_reference f_iri f_literal term =
    match term with
    | Base -> f_base ()
    | FragmentReference s -> f_fragment_reference s
    | Iri iri -> f_iri iri
    | Literal literal -> f_literal literal

  let compare a b =
    match (a, b) with
    | Base, Base -> 0
    | FragmentReference a, FragmentReference b -> String.compare a b
    | Iri a, Iri b -> Rdf.Iri.compare a b
    | Literal a, Literal b -> Rdf.Literal.compare a b
    | Base, _ -> -1
    | _, Base -> 1
    | FragmentReference _, _ -> -1
    | _, FragmentReference _ -> 1
    | Iri _, _ -> -1
    | _, Iri _ -> 1

  let to_cbor term =
    match term with
    | Base -> Cborl.Undefined
    | FragmentReference id -> Cborl.(Tag (Term.Iri.fragment_tag, TextString id))
    | Iri iri -> Term.Iri.to_cbor iri
    | Literal literal -> Term.Literal.to_cbor literal

  let of_cbor item =
    let open Cborl in
    match item with
    (* Base *)
    | Undefined -> Base
    (* Fragment Reference *)
    | Tag (tag, TextString id) when Z.(equal tag Term.Iri.fragment_tag) ->
        FragmentReference id
    (* IRI *)
    | Tag (tag, _)
      when Z.(
             equal tag Term.Iri.iri_tag
             || equal tag Term.Iri.uuid_tag
             || equal tag Term.Iri.eris_tag
             || equal tag Term.Iri.fragment_tag) ->
        of_iri @@ Term.Iri.of_cbor item
    (* Parse anything else as a literal *)
    | _ -> of_literal @@ Term.Literal.of_cbor item

  let pp ppf = function
    | Base -> Fmt.pf ppf "@[<5><base>@]"
    | FragmentReference id -> Fmt.pf ppf "@[<2><f %s>@]" id
    | Iri iri -> Fmt.pf ppf "@[<4><iri %a>@]" Rdf.Iri.pp iri
    | Literal lit -> Fmt.pf ppf "%a" Rdf.Literal.pp lit
end

module Subject = struct
  type internal
  type t = internal Term.internal

  let of_iri ?base_subject iri =
    match base_subject with
    | None -> Term.Iri iri
    | Some base_subject ->
        if Rdf.Iri.equal base_subject (get_base_subject iri) then
          match Rdf.Iri.fragment iri with
          | Some id -> Term.FragmentReference id
          | None -> Term.Base
        else Iri iri

  let base = Term.Base
  let make_fragment_reference id = Term.make_fragment_reference id

  let map f_base f_fr =
    Term.map f_base f_fr
      (fun _ -> raise @@ Invalid_argument "subject can not be a general iri")
      (fun _ -> raise @@ Invalid_argument "subject can not be literal")

  let to_term s = map (fun () -> Term.Base) Term.make_fragment_reference s
  let compare p1 p2 = Term.compare (to_term p1) (to_term p2)

  let expand ~base_subject =
    map
      (fun () -> Rdf.Triple.Subject.of_iri base_subject)
      (fun fragment ->
        Rdf.Iri.with_fragment base_subject (Some fragment)
        |> Rdf.Triple.Subject.of_iri)

  let pp = Term.pp
end

module Predicate = struct
  type internal
  type t = internal Term.internal

  let of_iri ?base_subject iri =
    match base_subject with
    | None -> Term.Iri iri
    | Some base_subject ->
        if Rdf.Iri.equal base_subject (get_base_subject iri) then
          match Rdf.Iri.fragment iri with
          | Some id -> Term.FragmentReference id
          | None -> Term.Base
        else Iri iri

  let base = Term.Base
  let make_fragment_reference id = Term.make_fragment_reference id

  let map f_base f_fr f_iri =
    Term.map f_base f_fr f_iri (fun _ ->
        raise @@ Invalid_argument "predicate can not be literal")

  let to_term p =
    map (fun () -> Term.Base) Term.make_fragment_reference Term.of_iri p

  let compare p1 p2 = Term.compare (to_term p1) (to_term p2)

  let expand ~base_subject =
    map
      (fun () -> Rdf.Triple.Predicate.of_iri base_subject)
      (fun fragment ->
        Rdf.Iri.with_fragment base_subject (Some fragment)
        |> Rdf.Triple.Predicate.of_iri)
      (fun iri -> Rdf.Triple.Predicate.of_iri iri)

  let pp = Term.pp
end

module Object = struct
  type internal
  type t = internal Term.internal

  let base = Term.Base

  let of_iri ?base_subject iri =
    match base_subject with
    | None -> Term.Iri iri
    | Some base_subject ->
        if Rdf.Iri.equal base_subject (get_base_subject iri) then
          match Rdf.Iri.fragment iri with
          | Some id -> Term.FragmentReference id
          | None -> Term.Base
        else Term.Iri iri

  let of_literal literal = Term.Literal literal
  let make_fragment_reference id = Term.FragmentReference id
  let map f_base f_fr f_iri f_literal = Term.map f_base f_fr f_iri f_literal

  let to_term p =
    map
      (fun () -> Term.Base)
      Term.make_fragment_reference Term.of_iri Term.of_literal p

  let compare p1 p2 = Term.compare (to_term p1) (to_term p2)

  let expand ~base_subject = function
    | Term.Base -> Rdf.Triple.Object.of_iri base_subject
    | Term.FragmentReference fragment ->
        Rdf.Iri.with_fragment base_subject (Some fragment)
        |> Rdf.Triple.Object.of_iri
    | Term.Iri iri -> iri |> Rdf.Triple.Object.of_iri
    | Term.Literal literal -> literal |> Rdf.Triple.Object.of_literal

  let pp = Term.pp
end

module Namespace = struct
  open Rdf.Namespace

  let a = rdf "type" |> Predicate.of_iri
  let value = rdf "value" |> Predicate.of_iri
end

module SubjectMap = Map.Make (Subject)
module PredicateMap = Map.Make (Predicate)
module ObjectSet = Set.Make (Object)

module PredicateObjects = struct
  type t = ObjectSet.t PredicateMap.t

  let equal = PredicateMap.equal ObjectSet.equal
  let empty = PredicateMap.empty
  let is_empty = PredicateMap.is_empty

  let union =
    PredicateMap.union (fun _p a b -> Option.some @@ ObjectSet.union a b)

  let singleton predicate object' =
    PredicateMap.singleton predicate @@ ObjectSet.singleton object'

  let add predicate object' predicate_objects =
    singleton predicate object' |> union predicate_objects

  let remove predicate object' predicate_objects =
    let remove_object object' objects =
      let objects = ObjectSet.remove object' objects in
      if ObjectSet.is_empty objects then None else Some objects
    in
    PredicateMap.update predicate
      (fun objects_opt -> Option.bind objects_opt (remove_object object'))
      predicate_objects

  let to_seq predicate_objects =
    PredicateMap.to_seq predicate_objects
    |> Seq.flat_map (fun (predicate, objects) ->
           ObjectSet.to_seq objects
           |> Seq.map (fun object' -> (predicate, object')))

  let to_nested_seq predicate_objects =
    PredicateMap.to_seq predicate_objects
    |> Seq.map (fun (predicate, objects) ->
           (predicate, ObjectSet.to_seq objects))
end

type fragment = PredicateObjects.t SubjectMap.t
type t = fragment

let empty = SubjectMap.empty
let singleton s p o = SubjectMap.singleton s @@ PredicateObjects.singleton p o

let of_triple ~base_subject (triple : Rdf.Triple.t) =
  let subject =
    Rdf.Triple.Subject.map
      (fun iri ->
        match Rdf.Iri.fragment iri with
        | None -> Subject.base
        | Some id -> Subject.make_fragment_reference id)
      (fun _bnode -> Subject.base)
      triple.subject
  in

  (* extract Predicate as Predicate *)
  let predicate =
    Rdf.Triple.Predicate.map
      (fun iri -> Predicate.of_iri ?base_subject iri)
      triple.predicate
  in

  (* extract object as Object *)
  let object' =
    Rdf.Triple.Object.map
      (fun iri -> Object.of_iri ?base_subject iri)
      (fun _bnode -> failwith "Blank Nodes are not allowed in Fragment Graphs.")
      Object.of_literal triple.object'
  in
  singleton subject predicate object'

let union =
  SubjectMap.union (fun _subject a b ->
      Option.some @@ PredicateObjects.union a b)

let add_statement predicate object' fragment =
  singleton Term.Base predicate object' |> union fragment

let add_opt_statement predicate object' fragment =
  match object' with
  | Some object' -> add_statement predicate object' fragment
  | None -> fragment

let add_fragment_statement fragment_id predicate object' fragment =
  singleton (Subject.make_fragment_reference fragment_id) predicate object'
  |> union fragment

let add_opt_fragment_statement fragment_id predicate object' fragment =
  match object' with
  | Some object' ->
      add_fragment_statement fragment_id predicate object' fragment
  | None -> fragment

let to_seq fragment =
  SubjectMap.to_seq fragment
  |> Seq.flat_map (fun (subject, predicate_objects) ->
         PredicateObjects.to_seq predicate_objects
         |> Seq.map (fun (predicate, object') -> (subject, predicate, object')))

let of_seq =
  Seq.fold_left
    (fun fragment (s, p, o) -> singleton s p o |> union fragment)
    empty

(* Constructor from triples *)

let of_triples seq () =
  let get_base (triple : Rdf.Triple.t) =
    Rdf.Triple.Subject.map
      (fun iri -> Rdf.Triple.Subject.of_iri @@ Rdf.Iri.with_fragment iri None)
      Rdf.Triple.Subject.of_blank_node triple.subject
  in

  let rec build_fragment running_base fragment seq =
    match Seq.uncons seq with
    | Some (triple, seq)
      when Rdf.Triple.Subject.(equal running_base @@ get_base triple) ->
        build_fragment running_base
          (of_triple
             ~base_subject:(Rdf.Triple.Subject.to_iri running_base)
             triple
          |> union fragment)
          seq
    | Some (triple, seq) ->
        let init_base = get_base triple in
        Seq.Cons
          ( (running_base, fragment),
            fun () ->
              build_fragment init_base
                (of_triple
                   ~base_subject:(Rdf.Triple.Subject.to_iri init_base)
                   triple)
                seq )
    | None -> Seq.Cons ((running_base, fragment), Seq.empty)
  in

  match Seq.uncons seq with
  | Some (triple, seq) ->
      let init_base = get_base triple in
      build_fragment init_base empty (Seq.cons triple seq)
  | None -> Seq.Nil

(* Serialization *)

module TermSet = Set.Make (Term)
module TermMap = Map.Make (Term)

module Dictionary = struct
  type t = { terms : Term.t array; lookup_map : int TermMap.t }

  let empty = { terms = [||]; lookup_map = TermMap.empty }

  (* Construct lookup map for terms *)
  let lookup_map terms =
    let _, lookup_map =
      Array.fold_left
        (fun (i, lookup) term -> (i + 1, TermMap.add term i lookup))
        (0, TermMap.empty) terms
    in
    lookup_map

  let of_fragment (fragment : fragment) =
    let subjects, pos =
      to_seq fragment
      |> Seq.fold_left
           (fun (subjects, pos) (s, p, o) ->
             ( subjects |> TermSet.add (Subject.to_term s),
               pos
               |> TermSet.add (Predicate.to_term p)
               |> TermSet.add (Object.to_term o) ))
           (TermSet.empty, TermSet.empty)
    in
    let terms =
      [ subjects; TermSet.diff pos subjects ]
      |> List.to_seq
      |> Seq.concat_map TermSet.to_seq
      |> Array.of_seq
    in
    { terms; lookup_map = lookup_map terms }

  (* Locate and Extract *)

  let locate { lookup_map; _ } term = TermMap.find term lookup_map
  let locate_opt { lookup_map; _ } term = TermMap.find_opt term lookup_map
  let extract { terms; _ } id = Array.get terms id

  (* Serializaiton *)

  let to_cbor { terms; _ } =
    terms |> Array.to_seq
    |> Seq.scan
         (fun (_, ie_prev) term ->
           let item, ie_prev' =
             match term with
             | Term.Iri iri -> Incremental_encoding.encode_iri ie_prev iri
             | _ -> (Term.to_cbor term, None)
           in
           (Some item, ie_prev'))
         (None, None)
    |> Seq.map fst
    |> Seq.filter_map (fun x -> x)
    |> List.of_seq
    |> fun l -> Cborl.Array l

  let of_cbor item =
    let term_to_iri_string term =
      Term.map
        (fun _ -> None)
        (fun _ -> None)
        (fun iri -> Option.some @@ Rdf.Iri.to_string iri)
        (fun _ -> None)
        term
    in
    match item with
    | Cborl.Array cbor_terms ->
        let terms =
          cbor_terms |> List.to_seq
          |> Seq.scan
               (fun (_, ie) item ->
                 match item with
                 | Cborl.Array [ Integer prefix_length; TextString suffix ]
                   when Option.is_some ie ->
                     let iri =
                       Incremental_encoding.decode ie prefix_length suffix
                     in
                     (Some (Term.of_iri iri), Some (Rdf.Iri.to_string iri))
                 | _ ->
                     let term = Term.of_cbor item in
                     (Some term, term_to_iri_string term))
               (None, None)
          |> Seq.map fst
          |> Seq.filter_map (fun x -> x)
          |> Array.of_seq
        in
        let lookup_map = lookup_map terms in
        { terms; lookup_map }
    | _ -> failwith "invalid encoding of dictionary"
end

module TermStream = struct
  let of_fragment dict fragment =
    SubjectMap.to_seq fragment
    |> Seq.map (fun (_subject, predicate_objects) ->
           predicate_objects |> PredicateObjects.to_nested_seq
           |> Seq.map (fun (predicate, objects) ->
                  ( Dictionary.locate dict @@ Predicate.to_term predicate,
                    objects
                    |> Seq.map (fun o ->
                           Dictionary.locate dict @@ Object.to_term o) ))
           |> Seq.unzip)
    |> Seq.unzip
    |> fun (p, o) -> (p, o |> Seq.concat)

  let decode dict predicates objects =
    let make_triple (s, p, o) : Subject.t * Predicate.t * Object.t =
      ( Term.map
          (fun () -> Subject.base)
          Subject.make_fragment_reference
          (fun _ -> raise @@ Invalid_argument "subject can not be iri")
          (fun _ -> raise @@ Invalid_argument "subject can not be literal")
          s,
        Term.map
          (fun () -> Predicate.base)
          Predicate.make_fragment_reference Predicate.of_iri
          (fun _ -> raise @@ Invalid_argument "subject can not be literal")
          p,
        Term.map
          (fun () -> Object.base)
          Object.make_fragment_reference Object.of_iri Object.of_literal o )
    in

    let subject_predicates =
      Seq.mapi
        (fun s_id ->
          let s = Dictionary.extract dict s_id in
          Seq.map (fun p_id ->
              let p = Dictionary.extract dict p_id in
              (s, p)))
        predicates
      |> Seq.concat
    in

    Seq.map2
      (fun (s, p) ->
        Seq.map (fun o_id ->
            let o = Dictionary.extract dict o_id in
            (s, p, o)))
      subject_predicates objects
    |> Seq.concat |> Seq.map make_triple
end

let content_addressable_molecule_tag = Z.(~$302)

let encode fragment =
  let dictionary = Dictionary.of_fragment fragment in
  let predicate_stream, object_stream =
    TermStream.of_fragment dictionary fragment
  in

  let predicate_bitmap, predicates = Molecule.Bitmap.encode predicate_stream in
  let object_bitmap, objects = Molecule.Bitmap.encode object_stream in

  Cborl.(
    Tag
      ( content_addressable_molecule_tag,
        Array
          [
            Dictionary.to_cbor dictionary;
            Integer predicate_bitmap;
            Array (predicates |> List.map (fun i -> Integer (Z.of_int i)));
            Integer object_bitmap;
            Array (objects |> List.map (fun i -> Integer (Z.of_int i)));
          ] ))

let int_of_cbor cbor =
  match cbor with
  | Cborl.Integer z -> Z.to_int z
  | _ -> failwith "expecting integer"

let decode cbor =
  match cbor with
  | Cborl.(
      Tag
        ( tag,
          Array
            [
              dict_cbor;
              Integer predicate_bitmap;
              Array predicates_cbor;
              Integer object_bitmap;
              Array objects_cbor;
            ] ))
    when Z.(equal tag content_addressable_molecule_tag) ->
      let dictionary = Dictionary.of_cbor dict_cbor in
      let predicate_stream =
        predicates_cbor |> List.map int_of_cbor |> Array.of_list
        |> Molecule.Bitmap.decode predicate_bitmap
      in
      let object_stream =
        objects_cbor |> List.map int_of_cbor |> Array.of_list
        |> Molecule.Bitmap.decode object_bitmap
      in

      let fragment =
        TermStream.decode dictionary predicate_stream object_stream |> of_seq
      in
      fragment
  | _ -> failwith "invalid content-addressable molecule"

(* Content-addressing *)

type hash = string -> Rdf.Iri.t

let base_subject ~hash fragment =
  fragment |> encode |> Cborl.write |> String.of_seq |> hash

let to_triples ~hash fragment =
  let base_subject = base_subject ~hash fragment in
  fragment |> to_seq
  |> Seq.map (fun (s, p, o) ->
         Rdf.Triple.make
           (Subject.expand ~base_subject s)
           (Predicate.expand ~base_subject p)
           (Object.expand ~base_subject o))

(* Pretty Printing *)

let pp_triple ppf (s, p, o) =
  Fmt.pf ppf "@[<v>(%a, %a, %a)@]" Subject.pp s Predicate.pp p Object.pp o

let pp ppf fragment_graph =
  Fmt.pf ppf "%a" (Fmt.seq pp_triple) (to_seq fragment_graph)
