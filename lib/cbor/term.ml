(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* This module implements the RDF/CBOR Term encoding. *)

(* Helper module for Hex encoding *)
module Hex = struct
  let hex_char x =
    assert (x >= 0 && x < 16);
    if x <= 9 then Char.chr @@ (Char.code '0' + x)
    else Char.chr @@ (Char.code 'A' + x - 10)

  let hex_value d =
    match d with
    | '0' -> 0
    | '1' -> 1
    | '2' -> 2
    | '3' -> 3
    | '4' -> 4
    | '5' -> 5
    | '6' -> 6
    | '7' -> 7
    | '8' -> 8
    | '9' -> 9
    | 'A' -> 10
    | 'B' -> 11
    | 'C' -> 12
    | 'D' -> 13
    | 'E' -> 14
    | 'F' -> 15
    | _ -> failwith "invalid hex digit"

  let to_hex s =
    let r = Bytes.create (String.length s * 2) in
    for i = 0 to String.length s - 1 do
      Bytes.set r (i * 2) @@ hex_char @@ (Char.code s.[i] lsr 4);
      Bytes.set r ((i * 2) + 1) @@ hex_char @@ (Char.code s.[i] land 0b1111)
    done;
    Bytes.to_string r

  let of_hex hex =
    String.init
      (String.length hex / 2)
      (fun i ->
        let hi = hex.[i * 2] |> hex_value in
        let lo = hex.[(i * 2) + 1] |> hex_value in
        Char.chr @@ ((hi lsl 4) lor lo))
end

module Iri = struct
  let iri_tag = Z.(~$266)
  let uuid_tag = Z.(~$37)
  let eris_tag = Z.(~$276)
  let fragment_tag = Z.(~$305)

  let uuid_to_cbor uuid =
    Cborl.(Tag (uuid_tag, ByteString (Uuidm.to_bytes uuid)))

  let eris_to_cbor read_cap = Cborl.(Tag (eris_tag, ByteString read_cap))

  let to_cbor (iri : Rdf.Iri.t) =
    let iri_s = Rdf.Iri.to_string iri in
    if String.starts_with ~prefix:"urn:uuid:" iri_s then
      match Uuidm.of_string ~pos:9 iri_s with
      | Some uuid -> (
          match Rdf.Iri.fragment iri with
          | None -> uuid_to_cbor uuid
          | Some fragment ->
              Cborl.(
                Tag
                  ( fragment_tag,
                    Array [ uuid_to_cbor uuid; TextString fragment ] )))
      | None -> Cborl.(Tag (iri_tag, TextString iri_s))
    else if String.starts_with ~prefix:"urn:eris:" iri_s then
      let read_cap = Base32.decode_exn ~off:9 ~len:106 iri_s in
      match Rdf.Iri.fragment iri with
      | None -> eris_to_cbor read_cap
      | Some fragment ->
          Cborl.(
            Tag
              ( fragment_tag,
                Array [ eris_to_cbor read_cap; TextString fragment ] ))
    else Cborl.(Tag (iri_tag, TextString (Rdf.Iri.to_string iri)))

  let rec of_cbor item =
    let open Cborl in
    match item with
    | Tag (tag, TextString iri) when Z.(equal tag iri_tag) ->
        Rdf.Iri.of_string iri
    | Tag (tag, ByteString s) when Z.(equal tag uuid_tag) -> (
        match Uuidm.of_bytes s with
        | Some uuid -> Rdf.Iri.of_string @@ "urn:uuid:" ^ Uuidm.to_string uuid
        | None -> failwith "invalid CBOR encoding of an UUID")
    | Tag (tag, ByteString read_cap) when Z.(equal tag eris_tag) ->
        Rdf.Iri.of_string @@ "urn:eris:" ^ Base32.encode_exn ~pad:false read_cap
    | Tag (tag, Array [ binary_iri; TextString fragment ])
      when Z.(equal tag fragment_tag) ->
        Rdf.Iri.with_fragment (of_cbor binary_iri) (Some fragment)
    | _ -> failwith "invalid CBOR encoding of an IRI"
end

module Literal = struct
  open Rdf.Namespace

  let generic_literal_tag = Z.(~$303)
  let lang_string_tag = Z.(~$38)

  let to_cbor (literal : Rdf.Literal.t) =
    let canonical = Rdf.Literal.canonical literal in
    match Rdf.Literal.datatype literal with
    (* Language-tagged string *)
    | datatype when Rdf.Iri.equal datatype (rdf "langString") ->
        let lang = Option.get @@ Rdf.Literal.language literal in
        Cborl.(
          Tag
            ( lang_string_tag,
              Array
                [ TextString lang; TextString (Rdf.Literal.canonical literal) ]
            ))
    (* XSD datatypes *)
    | datatype when Rdf.Iri.equal datatype (xsd "string") ->
        Cborl.TextString canonical
    | datatype when Rdf.Iri.equal datatype (xsd "boolean") -> (
        match canonical with
        | "true" -> Cborl.Bool true
        | "false" -> Cborl.Bool false
        | _ -> failwith "invalid xsd:boolean literal")
    | datatype when Rdf.Iri.equal datatype (xsd "integer") ->
        Cborl.Integer (Z.of_string canonical)
    | datatype when Rdf.Iri.equal datatype (xsd "float") ->
        Cborl.Float (Float.of_string canonical)
    | datatype when Rdf.Iri.equal datatype (xsd "double") ->
        Cborl.Double (Float.of_string canonical)
    | datatype when Rdf.Iri.equal datatype (xsd "dateTime") ->
        Cborl.Tag (Z.(~$0), Cborl.TextString canonical)
    | datatype when Rdf.Iri.equal datatype (xsd "hexBinary") ->
        Cborl.Tag (Z.(~$23), Cborl.ByteString (Hex.of_hex canonical))
    | datatype when Rdf.Iri.equal datatype (xsd "base64Binary") ->
        Cborl.ByteString (Base64.decode_exn canonical)
    (* Generic Literal *)
    | datatype ->
        Cborl.(
          Tag
            ( generic_literal_tag,
              Array
                [
                  Iri.to_cbor datatype;
                  TextString (Rdf.Literal.canonical literal);
                ] ))

  let of_cbor item =
    let open Cborl in
    match item with
    (* Language-tagged string *)
    | Tag (tag, Array [ TextString language; TextString canonical ])
      when Z.(equal tag lang_string_tag) ->
        Rdf.Literal.make_string ~language canonical
    (* XSD datatypes *)
    | TextString string -> Rdf.Literal.make_string string
    | Bool bool -> Rdf.Literal.make_boolean bool
    | Integer z -> Rdf.Literal.make_integer (Z.to_int z)
    | Float f -> Rdf.Literal.make_float f
    | Double f ->
        Rdf.Literal.make (Float.to_string f) (Rdf.Namespace.xsd "double")
    | Tag (tag, TextString dateTime) when Z.(equal tag ~$0) ->
        Rdf.Literal.make dateTime (Rdf.Namespace.xsd "dateTime")
    | Tag (tag, ByteString bytes) when Z.(equal tag ~$23) ->
        Rdf.Literal.make (Hex.to_hex bytes) (Rdf.Namespace.xsd "hexBinary")
    | ByteString bytes ->
        Rdf.Literal.make (Base64.encode_exn bytes)
          (Rdf.Namespace.xsd "base64Binary")
    (* Generic Literal *)
    | Tag (tag, Array [ iri_cbor; TextString value ])
      when Z.(equal tag generic_literal_tag) ->
        Rdf.Literal.make value (Iri.of_cbor iri_cbor)
    | _ -> failwith "invalid CBOR encoding of literal"
end

module Blank_node = struct
  let blank_node_tag = Z.(~$304)

  let to_cbor bnode =
    Cborl.Tag
      (blank_node_tag, Cborl.TextString (Rdf.Blank_node.identifier bnode))

  let of_cbor item =
    match item with
    | Cborl.Tag (tag, Cborl.TextString identifier)
      when Z.(equal tag blank_node_tag) ->
        Rdf.Blank_node.of_string identifier
    | _ -> failwith "invalid CBOR encoding of blank node"
end

let encode = Rdf.Term.map Iri.to_cbor Blank_node.to_cbor Literal.to_cbor

let decode item =
  let open Cborl in
  match item with
  (* IRI *)
  | Tag (tag, _)
    when Z.(
           equal tag Iri.iri_tag || equal tag Iri.uuid_tag
           || equal tag Iri.eris_tag || equal tag Iri.fragment_tag) ->
      Rdf.Term.of_iri @@ Iri.of_cbor item
  (* Blank node *)
  | Tag (tag, _) when Z.(equal tag Blank_node.blank_node_tag) ->
      Rdf.Term.of_blank_node @@ Blank_node.of_cbor item
  (* Parse anything else as a literal *)
  | _ -> Rdf.Term.of_literal @@ Literal.of_cbor item

let read signals =
  let item, signals = Cborl.Signal.to_item signals in
  (decode item, signals)
