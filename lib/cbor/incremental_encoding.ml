(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let lcp a b =
  Seq.map2 Char.equal (String.to_seq a) (String.to_seq b)
  |> Seq.take_while (fun x -> x)
  |> Seq.length

let encode_iri prev cur =
  let cur_s = Rdf.Iri.to_string cur in
  match prev with
  | Some prev ->
      let lcp = lcp prev cur_s in
      if lcp > 9 then
        ( Cborl.(
            Array
              [
                Integer (Z.of_int lcp);
                TextString (String.sub cur_s lcp (String.length cur_s - lcp));
              ]),
          Some cur_s )
      else (Term.Iri.to_cbor cur, Some cur_s)
  | None -> (Term.Iri.to_cbor cur, Some cur_s)

let decode prev prefix_length suffix =
  match prev with
  | Some prev ->
      Rdf.Iri.of_string (String.sub prev 0 (Z.to_int prefix_length) ^ suffix)
  | None ->
      raise
      @@ Invalid_argument
           "can not decode incremental encoded iri without previous state"
