(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021, 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let make_namespace base local = base ^ local |> Uri.of_string
let rdf = make_namespace "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
let rdfs = make_namespace "http://www.w3.org/2000/01/rdf-schema#"
let owl = make_namespace "http://www.w3.org/2002/07/owl#"
let xsd = make_namespace "http://www.w3.org/2001/XMLSchema#"
