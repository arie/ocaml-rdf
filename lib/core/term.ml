(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Iri = Uri

(* This is a phantom type. This allows a RDF term to appear in different
   contexts (as subject or as predicate) while keeping the exact same
   representation.*)
type 'a internal =
  | Iri of Iri.t
  | Blank_node of Blank_node.t
  | Literal of Literal.t

type any
type t = any internal

let of_iri iri = Iri iri
let to_iri = function Iri iri -> Some iri | _ -> None
let of_blank_node bnode = Blank_node bnode
let to_blank_node = function Blank_node bnode -> Some bnode | _ -> None
let of_literal literal = Literal literal
let to_literal = function Literal literal -> Some literal | _ -> None

let map f_iri f_blank_node f_literal term =
  match term with
  | Iri iri -> f_iri iri
  | Blank_node bnode -> f_blank_node bnode
  | Literal literal -> f_literal literal

let equal a b =
  match (a, b) with
  | Iri a, Iri b -> Iri.equal a b
  | Blank_node a, Blank_node b -> Blank_node.equal a b
  | Literal a, Literal b -> Literal.equal a b
  | _ -> false

let compare a b =
  match (a, b) with
  | Iri a, Iri b -> Iri.compare a b
  | Blank_node a, Blank_node b -> Blank_node.compare a b
  | Literal a, Literal b -> Literal.compare a b
  | Iri _, _ -> -1
  | _, Iri _ -> 1
  | Blank_node _, Literal _ -> -1
  | Literal _, Blank_node _ -> 1

let pp_iri ppf iri = Fmt.pf ppf "@[<4><iri@ %a>@]" Iri.pp iri

let pp ppf = function
  | Iri iri -> pp_iri ppf iri
  | Blank_node bnode -> Blank_node.pp ppf bnode
  | Literal literal -> Literal.pp ppf literal
