(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type state = { graph : Graph.t; node : Term.t }
type 'a t = state -> (state * 'a, string) Result.t

(* Combinators *)

let return v state = Result.ok (state, v)
let fail msg _ = Result.error msg
let map f p state = Result.map (fun (state, v) -> (state, f v)) (p state)
let bind f p state = Result.bind (p state) (fun (state, v) -> (f v) state)
let ( >>= ) p f = bind f p
let ( >>| ) p f = map f p
let ( <$> ) f p = p >>| f
let ( <*> ) f p = f >>= fun f -> p >>| f
let ( *> ) a b = a >>= fun _ -> b

let ( <* ) a b =
  a >>= fun x ->
  b >>| fun _ -> x

let ( <?> ) d msg state = d state |> Result.map_error (fun _ -> msg)
let ( <|> ) p q state = match p state with Ok v -> Ok v | Error _ -> q state

let choice ?(failure_msg = "no more choices") ps =
  List.fold_right ( <|> ) ps (fail failure_msg)

let option p = p >>| Option.some <|> return None

(* Term decoders *)

let any_iri state =
  match Term.to_iri state.node with
  | Some iri -> Ok (state, iri)
  | None -> Error "not an IRI"

let iri v state =
  match Term.to_iri state.node with
  | Some iri -> if Uri.equal iri v then Ok (state, ()) else Error "wrong iri"
  | None -> Error "not an IRI"

let any_literal state =
  match Term.to_literal state.node with
  | Some literal -> Ok (state, literal)
  | None -> Error "not a literal"

let any_blank_node state =
  match Term.to_blank_node state.node with
  | Some blank_node -> Ok (state, blank_node)
  | None -> Error "not a blank node"

let any_term state = Ok (state, state.node)

(* Graph combinators *)

let subject_of_term term =
  Term.map
    (fun iri -> Ok (Triple.Subject.of_iri iri))
    (fun blank_node -> Ok (Triple.Subject.of_blank_node blank_node))
    (fun _ -> Error "cannot cast literal to subject")
    term

let subject state =
  subject_of_term state.node |> Result.map (fun subject -> (state, subject))

let objects predicate (d : 'a t) state =
  let decode_object object' =
    match d { state with node = Triple.Object.to_term object' } with
    | Ok (_, v) -> Some v
    | Error _ -> None
  in
  subject_of_term state.node
  |> Result.map (fun subject ->
         ( state,
           Graph.objects subject predicate state.graph
           |> Seq.filter_map decode_object ))

let functional_property predicate (d : 'a t) state =
  Result.bind (subject_of_term state.node) (fun subject ->
      match Graph.functional_property subject predicate state.graph with
      | Some object' -> d { state with node = Triple.Object.to_term object' }
      | None -> Error "no value for functional property")

(* RDF Combinators *)

let type' type_iri =
  objects (Triple.Predicate.of_iri (Namespace.rdf "type")) (iri type_iri)
  >>= fun seq ->
  if Seq.fold_left (fun _ _ -> true) false seq then return ()
  else fail "node does not have required type"

(* Decoding *)

let decode subject (decoder : 'a t) graph =
  decoder { graph; node = Triple.Subject.to_term subject } |> Result.map snd
