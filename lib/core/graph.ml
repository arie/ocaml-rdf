(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* This is a simple functional RDF graph structure using `Map`s and `Set`s.

   This allows efficient quering in the order subject -> predicate -> object.
   But not in any other.This can be improved.
*)

module SubjectMap = Map.Make (Triple.Subject)
module PredicateObjects = Description.PredicateObjects

type t = PredicateObjects.t SubjectMap.t

let equal = SubjectMap.equal PredicateObjects.equal
let empty = SubjectMap.empty

let singleton (triple : Triple.t) =
  SubjectMap.singleton triple.subject
  @@ PredicateObjects.singleton triple.predicate triple.object'

let union =
  SubjectMap.union (fun _subject a b ->
      Option.some @@ PredicateObjects.union a b)

let add (triple : Triple.t) graph = triple |> singleton |> union graph

let add_seq triples graph =
  Seq.fold_left (fun graph triple -> add triple graph) graph triples

let remove (triple : Triple.t) graph =
  let remove_po predicate object' predicate_objects =
    let predicate_objects =
      PredicateObjects.remove predicate object' predicate_objects
    in
    if PredicateObjects.is_empty predicate_objects then None
    else Some predicate_objects
  in
  SubjectMap.update triple.subject
    (fun predicate_objects_opt ->
      Option.bind predicate_objects_opt
        (remove_po triple.predicate triple.object'))
    graph

let subjects graph =
  SubjectMap.to_seq graph |> Seq.map (fun (s, _predicate_objects) -> s)

let description subject graph : Description.t =
  match SubjectMap.find_opt subject graph with
  | Some predicate_objects -> { subject; predicate_objects }
  | None -> Description.empty subject

let descriptions graph : Description.t Seq.t =
  SubjectMap.to_seq graph
  |> Seq.map (fun (subject, predicate_objects) : Description.t ->
         { subject; predicate_objects })

let objects subject predicate graph =
  description subject graph |> Description.objects predicate

let functional_property subject predicate graph =
  description subject graph |> Description.functional_property predicate

let collection subject predicate graph =
  (* helper to get the first value of a sequence *)
  let seq_next seq =
    match seq () with Seq.Cons (value, _) -> Some value | Seq.Nil -> None
  in

  let to_subject object' =
    object' |> Triple.Object.to_term |> fun term ->
    Term.map Triple.Subject.of_iri Triple.Subject.of_blank_node
      (fun _literal ->
        raise
        @@ Invalid_argument "encountered literal while following collection")
      term
  in

  let get_first node =
    objects node (Triple.Predicate.of_iri @@ Namespace.rdf "first") graph
    |> seq_next
    |> Option.map Triple.Object.to_term
  in

  let get_rest node =
    objects node (Triple.Predicate.of_iri @@ Namespace.rdf "rest") graph
    |> seq_next |> Option.map to_subject
  in

  let rec follow node () =
    match node with
    | Some node -> (
        match get_first @@ node with
        | Some term -> Seq.Cons (term, follow @@ get_rest @@ node)
        | None -> Seq.Nil)
    | None -> Seq.Nil
  in

  let initial_node =
    objects subject predicate graph |> seq_next |> Option.map to_subject
  in

  follow initial_node

let of_triples seq = add_seq seq empty

let to_triples graph =
  SubjectMap.to_seq graph
  |> Seq.flat_map (fun (subject, predicate_objects) ->
         PredicateObjects.to_seq predicate_objects
         |> Seq.map (fun (predicate, object') ->
                Triple.make subject predicate object'))

let pp ppf graph = Fmt.pf ppf "%a" (Fmt.seq Triple.pp) (to_triples graph)

let to_nested_seq graph =
  SubjectMap.to_seq graph
  |> Seq.map (fun (s, predicate_objects) ->
         (s, PredicateObjects.to_nested_seq predicate_objects))
