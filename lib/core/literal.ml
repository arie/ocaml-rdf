(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Iri = Uri

type t = { value : string; datatype : Iri.t; language : string option }

let make value datatype = { value; datatype; language = None }

let make_string ?language value =
  match language with
  | Some language ->
      { value; language = Some language; datatype = Namespace.rdf "langString" }
  | None -> make value (Namespace.xsd "string")

let make_boolean value =
  if value then make "true" (Namespace.xsd "boolean")
  else make "false" (Namespace.xsd "boolean")

let make_float value = make (Float.to_string value) (Namespace.xsd "float")
let make_integer value = make (Int.to_string value) (Namespace.xsd "integer")
let canonical literal = literal.value
let datatype literal = literal.datatype
let language literal = literal.language

let equal a b =
  String.equal a.value b.value
  && Iri.equal a.datatype b.datatype
  && Option.equal String.equal a.language b.language

let compare a b =
  String.compare
    (Iri.to_string a.datatype ^ canonical a
    ^ Option.value ~default:"" a.language)
    (Iri.to_string b.datatype ^ canonical b
    ^ Option.value ~default:"" b.language)

let pp ppf literal =
  match literal.language with
  | None ->
      Fmt.pf ppf "@[<h 1><literal@ \"%s\"^^%a>@]" (canonical literal) Iri.pp
        (datatype literal)
  | Some language ->
      Fmt.pf ppf "@[<h 1><literal@ \"%s\"@%s>@]" (canonical literal) language
