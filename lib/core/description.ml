(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module PredicateMap = Map.Make (Triple.Predicate)
module ObjectSet = Set.Make (Triple.Object)

module PredicateObjects = struct
  type t = ObjectSet.t PredicateMap.t

  let equal = PredicateMap.equal ObjectSet.equal
  let empty = PredicateMap.empty
  let is_empty = PredicateMap.is_empty

  let union =
    PredicateMap.union (fun _p a b -> Option.some @@ ObjectSet.union a b)

  let singleton predicate object' =
    PredicateMap.singleton predicate @@ ObjectSet.singleton object'

  let add predicate object' predicate_objects =
    singleton predicate object' |> union predicate_objects

  let remove predicate object' predicate_objects =
    let remove_object object' objects =
      let objects = ObjectSet.remove object' objects in
      if ObjectSet.is_empty objects then None else Some objects
    in
    PredicateMap.update predicate
      (fun objects_opt -> Option.bind objects_opt (remove_object object'))
      predicate_objects

  let to_seq predicate_objects =
    PredicateMap.to_seq predicate_objects
    |> Seq.flat_map (fun (predicate, objects) ->
           ObjectSet.to_seq objects
           |> Seq.map (fun object' -> (predicate, object')))

  let to_nested_seq predicate_objects =
    PredicateMap.to_seq predicate_objects
    |> Seq.map (fun (predicate, objects) ->
           (predicate, ObjectSet.to_seq objects))
end

type t = { subject : Triple.Subject.t; predicate_objects : PredicateObjects.t }

let equal a b =
  Triple.Subject.equal a.subject b.subject
  && PredicateObjects.equal a.predicate_objects b.predicate_objects

let subject description = description.subject
let empty subject = { subject; predicate_objects = PredicateObjects.empty }

let add predicate object' description =
  {
    description with
    predicate_objects =
      PredicateObjects.add predicate object' description.predicate_objects;
  }

let objects predicate description =
  match PredicateMap.find_opt predicate description.predicate_objects with
  | None -> Seq.empty
  | Some objects -> ObjectSet.to_seq objects

let functional_property predicate description =
  match PredicateMap.find_opt predicate description.predicate_objects with
  | None -> None
  | Some objects -> ObjectSet.choose_opt objects

let functional_property_iri predicate description =
  Option.bind (functional_property predicate description) Triple.Object.to_iri

let functional_property_literal predicate description =
  Option.bind
    (functional_property predicate description)
    Triple.Object.to_literal

let to_triples description =
  PredicateMap.to_seq description.predicate_objects
  |> Seq.flat_map (fun (predicate, objects) ->
         ObjectSet.to_seq objects
         |> Seq.map (fun object' ->
                Triple.make description.subject predicate object'))

let to_nested_seq description =
  PredicateObjects.to_nested_seq description.predicate_objects
