(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** This module provides a parser for reading triple from the RDF/XML syntax.

@see <https://www.w3.org/TR/rdf-syntax-grammar/>

 *)

(** {1 Parser} *)

val parse :
  ?base_iri:Rdf.Iri.t ->
  ?seed:Random.State.t ->
  f:(Rdf.Triple.t -> unit) ->
  Xmlm.signal Seq.t ->
  unit
(** [parse ~base_iri ~seed ~f signals] reads the sequence of XML signals in [signals], parses signals as an RDF/XML document and emits all triples with [f].

Note that the sequence of signals must be persistent in order to allow the parser to lookahead and backtrack efficiently. If your sequence is not persistent you may use {!val persisten_seq} to make the sequence persistent.
     
@raises Invalid_argument on invalid input
*)

val parse_to_graph :
  ?base_iri:Rdf.Iri.t ->
  ?seed:Random.State.t ->
  Xmlm.signal Seq.t ->
  Rdf.Graph.t
(** [parse_to_graph ~base_iri ~seed signals] reads the sequence of XML signals in [signals] and builds a graph holding all triples appearing in the RDF/XML document. 

Note that the sequence of signals must be persistent in order to allow the parser to lookahead and backtrack efficiently. If your sequence is not persistent you may use {!val persisten_seq} to make the sequence persistent.

@raises Invalid_argument on invalid input
*)

(** {2 Input helpers} *)

val persistent_seq : 'a Seq.t -> 'a Seq.t
(** [persistent_seq seq] makes a transient sequence persistent by lazily storing computed values on initial call and returning the stored value on subsequent calls. *)

val xmlm_input_to_seq : Xmlm.input -> Xmlm.signal Seq.t
(** [xmlm_input_to_seq input] returns a persistent sequence of XML signals that will read from the Xmlm input [input]. *)

(** {1 Encoding} *)

val to_signals :
  ?prefixes:(string * Rdf.Iri.t) list -> Rdf.Graph.t -> Xmlm.signal Seq.t
(** [to_signals ~prefixes graph] returns a sequence containg XML signals of all statements in [graph] serialized using RDF/XML.

If [prefixes] is provides a XML namespace definitions will be added to the root RDF element and all matching RDF IRIs will use the XML namespaces.

Note that no `Dtd signal is added to the beginning of the sequence. You must manually add this if you want to output the signals with Xmlm. *)

val signals_of_description :
  ?prefixes:(string * Rdf.Iri.t) list -> Rdf.Description.t -> Xmlm.signal Seq.t
(** [signals_of_description description] returns a sequence of XML signals that encode the given [description]. This is useful when serializing large amounts of RDF data as the XML signals can be emitted immediately. *)
